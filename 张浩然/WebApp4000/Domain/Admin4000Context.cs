﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp4000.Domain
{
    public class Admin4000Context:DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=.;database=Admin4000;uid=sa;pwd=123456");
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<Brand> Brands { get; set; }
    }
}
